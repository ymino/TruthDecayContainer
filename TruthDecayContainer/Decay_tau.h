#ifndef DECAY_TAU_h
#define DECAY_TAU_h

//+++++++++++++++++++ include STL +++++++++++++++++++++//
#include <vector>

//++++++++++++++++++ include ROOT lib ++++++++++++++++++++++//
#include "TLorentzVector.h"
#include "TObject.h"

//+++++++++++++++++++ include Ex lib +++++++++++++++++++++//
#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;


class Decay_tau : public TObject {

 public:

  Decay_tau();

  ~Decay_tau();

  Decay_tau(const Decay_tau &rhs);
    
  void finalize();

  static TString getDecayString(int in_decayLabel);
  static TString getDecayString_tautau(int decayLabel1, int decayLabel2);

  void print();

  void clear();

  ClassDef(Decay_tau,1);

  // tlv
  tlv P4;
  tlv pMis;  // met source

  // children
  tlv pchild, pchild2, ptaunu;

  // decay label 
  int  decayLabel;  

  // pdg
  int pdg, child_pdg, child2_pdg;

  // ++++ derived variables

  // angles
  double coschild_tau;

 private:  




};







#endif
