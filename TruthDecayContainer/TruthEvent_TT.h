#ifndef TRUTHEVENT_TT_h
#define TRUTHEVENT_TT_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_H.h"
#include "TruthDecayContainer/TruthEvent_VV.h"


typedef TLorentzVector tlv;


class TruthEvent_TT : public TruthEvent_VV {

 public:


  TruthEvent_TT();

  ~TruthEvent_TT();

  TruthEvent_TT(const TruthEvent_TT &rhs);

  // Method to set 
  virtual void set_evt(const tlv &in_pb1, const Decay_boson &W1, const tlv &in_pb2, const Decay_boson &W2);

  // If you want set only the WW part... 
  virtual void set_evt(const Decay_boson &W1, const Decay_boson &W2);

  // Method to set an additional boson besides ttbar
  void add_W(const Decay_boson &W);
  void add_Z(const Decay_boson &Z);
  void add_H(const Decay_H &H);

  // Calculate event-level variables based on the legs set  
  virtual void finalize();

  // If you want to force W2 to always decays leptonically in case of semil-leptonic ttbar....
  virtual void check_swap();

  // Swap t1 <-> t2 
  virtual void swap12();

  // Print the contents
  virtual void print();

  // Reset all
  virtual void clear();

  

  // ^^^^^^^^^^^^^^^^^ member variables ^^^^^^^^^^^^^^^^ //

  // ++++++ tlv  +++++ //
  tlv pb1, pt1, pb2, pt2, ptt;

  // ++++ derived variables ++++++ //
  double cost_TT, cosW1_t1, cosW2_t2;

  // ++++ additional boson X (e.g. ttW, ttZ, ttH) ++++++ //
  tlv pX;
  int X_pdg;

  // X's children info
  int XdecayLabel;
  tlv pXq1, pXq2;
  int Xq1_pdg, Xq2_pdg;

  // X's grandchildren info
  int XdecayLabel_sub1, XdecayLabel_sub2;
  tlv pXq11, pXq12, pXq21, pXq22;
  int Xq11_pdg, Xq12_pdg,Xq21_pdg, Xq22_pdg;


  ClassDef(TruthEvent_TT,3);

 private:  



};







#endif
